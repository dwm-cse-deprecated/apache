#! /bin/bash

set -ex

target_dir=`cd $(dirname ${0}) && pwd`

yum -y install tar
yum -y install make gcc-c++
yum -y install perl expat expat-devel

cd /usr/local/src
curl -L  http://ftp.jaist.ac.jp/pub/apache//apr/apr-1.5.2.tar.gz -o apr-1.5.2.tar.gz
tar zxvf apr-1.5.2.tar.gz
cd apr-1.5.2
./configure && make && make install

cd /usr/local/src
curl -L http://ftp.jaist.ac.jp/pub/apache//apr/apr-util-1.5.4.tar.gz -o apr-util-1.5.4.tar.gz
tar zxvf apr-util-1.5.4.tar.gz
cd apr-util-1.5.4
./configure --with-apr=/usr/local/apr && make && make install

cd /usr/local/src
curl -L ftp://ftp.csx.cam.ac.uk/pub/software/programming/pcre/pcre-8.38.tar.gz -o pcre-8.38.tar.gz
tar zxvf pcre-8.38.tar.gz
cd pcre-8.38
./configure && make && make install

ldconfig

cd /usr/local/src
curl -L http://ftp.jaist.ac.jp/pub/apache//httpd/httpd-2.4.18.tar.gz -o httpd-2.4.18.tar.gz
tar zxvf httpd-2.4.18.tar.gz
cd httpd-2.4.18

cp -a /usr/local/src/apr-1.5.2 srclib/apr
cp -a /usr/local/src/apr-util-1.5.4 srclib/apr-util

./configure --enable-so \
--enable-rewrite \
--enable-expires \
--enable-proxy \
--enable-headers \
--enable-logio \
--disable-ssl \
--disable-deflate \
--disable-cache \
--disable-disk-cache \
--disable-auth-basic \
--disable-authn-file \
--disable-authn-default \
--disable-authz-groupfile \
--disable-authz-user \
--disable-filter \
--disable-autoindex \
--with-pcre=/usr/local/bin/pcre-config \
--with-included-apr \
--with-mpm=prefork
make && make install

# create user
groupadd httpd
useradd -g httpd httpd

cp ${target_dir}/configs/httpd.conf /usr/local/apache2/conf/httpd.conf
cp ${target_dir}/configs/httpd-default.conf /usr/local/apache2/conf/extra/httpd-default.conf
cp ${target_dir}/configs/httpd-info.conf /usr/local/apache2/conf/extra/httpd-info.conf
cp ${target_dir}/configs/httpd-mpm.conf /usr/local/apache2/conf/extra/httpd-mpm.conf
cp ${target_dir}/configs/httpd-vhosts.conf /usr/local/apache2/conf/extra/httpd-vhosts.conf
cp ${target_dir}/configs/init.d /etc/rc.d/init.d/httpd

chmod 755 /etc/rc.d/init.d/httpd
chkconfig --add httpd

mkdir /var/log/apache
chown -R httpd:httpd /var/log/apache

mkdir /var/run/httpd

echo "export PATH=\$PATH:/usr/local/apache2/bin" > /etc/profile.d/apache.sh
